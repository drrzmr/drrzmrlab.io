addCommandAlias(
  name = "doFormat",
  value = """
            |; scalafmtSbt
            |; scalafmt
            |; test:scalafmt
            |; scalafixAll
            |""".stripMargin
)

addCommandAlias(
  name = "checkFormat",
  value = """
            |; scalafmtSbtCheck
            |; scalafmtCheckAll
            |; test:scalafmtCheckAll
            |; scalafixAll --check""".stripMargin
)

addCommandAlias(
  name = "checkUpdates",
  value = """
            |; reload plugins; dependencyUpdates; reload return
            |; project base; dependencyUpdates""".stripMargin
)
