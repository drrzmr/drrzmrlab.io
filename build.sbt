enablePlugins(GitVersioning, GitBranchPrompt)

Global / onChangedBuildSource := ReloadOnSourceChanges

ThisBuild / organization := "io.gitlab.drrzmr"
ThisBuild / scalaVersion := "2.13.5"
ThisBuild / organizationName := "drrzmr.gitlab.io"
ThisBuild / organizationHomepage := Some(url("https://drrzmr.gitlab.io"))
ThisBuild / homepage := Some(url("https://drrzmr.gitlab.io"))
ThisBuild / startYear := Some(2021)
ThisBuild / licenses += "Apache 2.0" -> url("https://www.apache.org/licenses/LICENSE-2.0")

/* scalafix */
ThisBuild / semanticdbEnabled := true
ThisBuild / semanticdbVersion := scalafixSemanticdb.revision
ThisBuild / scalafixScalaBinaryVersion := "2.13"
ThisBuild / scalafixDependencies += "com.github.liancheng" %% "organize-imports" % "0.5.0"

/* compiler */
ThisBuild / scalacOptions ++= Seq(
  "-Wunused" /* required by scalafix */
)

lazy val root = (project in file("."))
  .enablePlugins(BuildInfoPlugin)
  .enablePlugins(SbtTwirl)
  .settings(
    /* build info */
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
    buildInfoPackage := "sbt.buildinfo",
    buildInfoObject := "pluginData",
    buildInfoOptions += BuildInfoOption.BuildTime,
	/* project */
    name := "drrzmr"
  )

/* - for doc purposes
 * sourceDirectories in (Compile, TwirlKeys.compileTemplates) := (unmanagedSourceDirectories in Compile).value
 * Compile / unmanagedSourceDirectories += crossTarget.value / "twirl"
 */
