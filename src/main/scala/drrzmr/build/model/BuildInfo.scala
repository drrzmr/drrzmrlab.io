package drrzmr.build.model

case class BuildInfo(version: String,
                     scalaVersion: String,
                     sbtVersion: String,
                     builtAtString: String,
                     builtAtMillis: Long)
