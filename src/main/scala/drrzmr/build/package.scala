package drrzmr

import drrzmr.build.model.BuildInfo

package object build {

  def load: BuildInfo =
    BuildInfo(
      version = sbt.buildinfo.pluginData.version,
      scalaVersion = sbt.buildinfo.pluginData.scalaVersion,
      sbtVersion = sbt.buildinfo.pluginData.sbtVersion,
      builtAtString = sbt.buildinfo.pluginData.builtAtString,
      builtAtMillis = sbt.buildinfo.pluginData.builtAtMillis
    )
}
